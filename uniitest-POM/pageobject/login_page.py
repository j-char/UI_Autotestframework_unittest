from lib2to3.pgen2 import driver

from selenium.webdriver.common.by import By
from base.basepage import BasePage
from logging_module.logging_module import logging_module

mylogging = logging_module(logger='Login_Page').getlog()


class Login_Page(BasePage):
    # 类=属性+方法
    # 属性
    username = (By.ID, "loginname")
    password = (By.ID, "nloginpwd")
    login_btn = (By.ID, "loginsubmit")
    click_userlogin = (By.CSS_SELECTOR, "div.login-tab:nth-child(3) > a:nth-child(1)")
    url = "https://passport.jd.com/new/login.aspx?ReturnUrl=https%3A%2F%2Fwww.jd.com%2F%3Fcu%3Dtrue%26utm_source" \
          "%3Dbaidu-pinzhuan%26utm_medium%3Dcpc%26utm_campaign%3Dt_288551095_baidupinzhuan%26utm_term" \
          "%3D0f3d30c8dba7459bb52f2eb5eba8ac7d_0_1d323ab73d174c4e8625daec2d7afbc5 "

    def login(self, username, password):
        # 方法  主要操作流程封装成一个方法
        self.geturl(self.url)
        mylogging.info(u"打开京东网页")
        self.click(self.click_userlogin)
        mylogging.info(u"切换京东账号登录")
        self.input(self.username, username)
        mylogging.info(u"输入用户名:" + username, )
        self.input(self.password, password)
        mylogging.info(u"输入密码:******")
        self.click(self.login_btn)
        mylogging.info(u"点击提交")

