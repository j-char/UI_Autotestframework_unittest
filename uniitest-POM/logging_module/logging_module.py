# _*_ coding:utf-8 _*_
import logging
import os.path
import time
import os


class logging_module(object):
    def __init__(self, logger):
        # '指定保存日志的文件路径，日志级别，以及调用文件将日志存入到指定文件中'
        #  创建一个logger
        self.logger = logging.getLogger(logger)
        self.logger.setLevel(logging.DEBUG)
        # 创建一个handler,用于写入到日志文件
        rq = time.strftime('%Y%m%d%H%M', time.localtime(time.time()))
        # os.getcwd()当前的目录，os.path.dirname当前路径,此文件夹就在桌面，当前位置是desktop
        log_path = os.path.dirname(os.getcwd()) + '/uniitest-POM/report_log/Logs'
        log_name = log_path + '.log'
        fh = logging.FileHandler(log_name, encoding='utf-8')
        fh.setLevel(logging.INFO)
        # 创建一个handler，用于输出到控制台
        ch = logging.StreamHandler()
        ch.setLevel(logging.INFO)

        # 定义handler的输出格式
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s -s%(message)s')
        fh.setFormatter(formatter)
        ch.setFormatter(formatter)

        # 给logger添加handler
        self.logger.addHandler(fh)
        self.logger.addHandler(ch)

    def getlog(self):
        return self.logger
