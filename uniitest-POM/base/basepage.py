import time


class BasePage:
    def __init__(self, driver) -> None:
        self.driver = driver

    def locator(self, loc) -> None:
        return self.driver.find_element(*loc)

    def input(self, loc, value) -> None:
        self.locator(loc).send_keys(value)

    def click(self, loc) -> None:
        self.locator(loc).click()

    def geturl(self, url) -> None:
        self.driver.get(url)

