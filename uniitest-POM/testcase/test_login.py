import imp
from selenium.webdriver.common.by import By
import unittest
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from pageobject.login_page import Login_Page
from ddt import ddt, file_data, unpack
from pageobject.login_page import mylogging


@ddt
class LoginCase(unittest.TestCase):
    # 将驱动放入python路径和firefox路径下、设置firefox浏览器的高级配置--环境变量PATH。
    # binary = FirefoxBinary('C:/Users/J-Char/AppData/Local/Mozilla Firefox/geckodriver.exe')
    def setUp(self):
        self.driver = webdriver.Firefox()

    @file_data("C:/Users/J-Char/Desktop/uniitest-POM/testdata/test-json.json")
    @unpack
    def test_login(self, username, password):
        Log = Login_Page(self.driver)
        Log.login(username, password)
        # 智能等待元素出现
        element = WebDriverWait(self.driver, 10, 0.5).until(EC.visibility_of_element_located((By.XPATH, '/html/body'
                                                                                                        '/div['
                                                                                                        '4]/div/div'
                                                                                                        '/div/div['
                                                                                                        '1]/div['
                                                                                                        '1]/div[1]')))
        var = element.text
        # self.assertEqual(var, u'完成拼图验证')
        # 断言标签内容。
        self.assertEqual(var, u'完成拼图验证')
        mylogging.info("断言成功")


    def tearDown(self) -> None:
        self.driver.close()
        mylogging.info("关闭浏览器")
