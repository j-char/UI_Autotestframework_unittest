import unittest
from HTMLtestRunnerScreenShot import HTMLTestRunner
import yagmail
from testcase.test_login import LoginCase
import time
from testcase import *

# UI框架：pom设计（关键字驱动、数据驱动、页面对象）+日志封装+断言+测试报告+邮件发送
test_dir = './'
discover = unittest.defaultTestLoader.discover(test_dir, pattern='testcase')


def send_mail(report):
    yag = yagmail.SMTP(user="1743199431@qq.com",
                       password="supergood..2019",
                       host='mail.qq.com')
    subject = "主题，自动化测试报告"
    contents = "正文，请查看附件"
    yag.send(['GZC1743199431@163.com'], subject, contents, report)
    print('email has send out !')


if __name__ == '__main__':
    # HTMLtestRunnerScreenShot.py  将文件保存到Python解释器的Lib\site-packages中。然后在测试脚本中正常的调用即可。
    suite = unittest.makeSuite(LoginCase)
    # 取当前日期时间
    now_time = time.strftime("%Y-%m-%d %H_%M_%S")
    html_report = '/report_log/' + now_time + 'report.html'
    f = open('report_log/' + now_time + 'report.html', 'wb')
    HTMLTestRunner(
        stream=f,
        verbosity=2,
        title='京东登录测试报告',
        description='判断title是否符合预期',
        tester='gzc',  # 测试者
    ).run(suite)
    f.close()
    send_mail(html_report)
